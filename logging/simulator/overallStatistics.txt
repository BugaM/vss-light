# Number of Matches: 11

-------  Goals -------
>> Left Team <<
# Average number: 0
# Standard deviation: 0
>> Right Team <<
# Average number: 0
# Standard deviation: 0

------- Penalty Fouls -------
>> Left Team <<
# Average number: 0
# Standard deviation: 0
>> Right Team <<
# Average number: 0.1
# Standard deviation: 0.316228

------- Attack Fouls -------
>> Left Team <<
# Average number: 0.7
# Standard deviation: 0.674949
>> Right Team <<
# Average number: 0
# Standard deviation: 0

------- Ball Possession -------
>> Left Team <<
# Average percentage: 99.3352
# Standard deviation: 2.10235
>> Right Team <<
# Average percentage: 0.66482
# Standard deviation: 2.10235

------- Free Balls -------
# Average number: 0
# Standard deviation: 0


# Left team wins: 0
# Right team wins: 0
# Ties: 10

