# ITAndroids VSS

Source code for the ITAndroids Very Small Size Team

### Installation guide for Ubuntu 16.04
* Download the itandroids-lib library in the same folder of the itandroids-vss library. 
* Install all dependencies, running the scripts/install/16.04/install_dependencies.sh.
* Install the flycapture api, following the instructions of scripts/install/16.04/Doc Instalação do VSS.txt

### Installation guide for Ubuntu 18.04
* Download the itandroids-lib library in the same folder of the itandroids-vss library. 
* Install all dependencies, running the scripts/install/18.04/install\_dependencies.sh.
* Install the flycapture api, following the instructions of scripts/install/18.04/manual_instalacao/step3-flycapture.txt

### Building and running
* Command Line

From the project root folder:

```sh
$ mkdir build
$ cd build
$ cmake ..
$ make -j4
```

 Then, go to the binaries folder, where the executables are installed and run the code with:
 
```sh
$ ./agent
```

### Directory structure

* binaries - binary and common configuration files
* documentation - convention and information documents
* source/cmake - additional cmake scripts
* source/core - main vss application
* source/tests
* source/tools - calibration and simulation tools

### Rules

* Before commiting on main branches (MASTER, DEVELOPMENT, STABLE), you MUST run the simulator and it can't crash.
* Don't break tests! Run build all to check if you've broken anything before commiting.
* Branch DEVELOPMENT: must work on simulator.
* Branch MASTER: must work on the real robots
* Branch STABLE: must have worked very well in real games
* Personal remote branches: can be created for feature development, but it's name has to be clearly related with the feature. The branch shuld be deleted as soon as it isn't useful anymore.
# vss-light
